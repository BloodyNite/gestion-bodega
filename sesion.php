<!-- Evaluar que la sesión continue, verificando la variable de sesión creada para este propósito.
	Si la variable cambió su valor inicial se enviará la variable error=si al archivo salir.php -->
<?php
// Se verifica que el valor inicial de la variable de sesión creada para este propósito al momento de iniciar sesión mantenga su valor, si su valor no es el mismo enviará variable al archivo salir.php.
session_start();
if(!$_SESSION["activo"]){
header("Location:salir.php?error=si");
}
?>
